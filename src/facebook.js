FB.init({
    appId: '1812058792412901',
    xfbml: true,
    version: 'v2.7'
});

var friends = [];
function onConnected() {
    FB.api('/me?fields=id,name,email,friends', function(response) {
        if (!response.email) {
            //return ngToast.create({
            //    className: 'warning',
            //    content: 'Não foi possível acessar o seu email.'
            //});
        }

        for (var i = 0; i < response.friends.data.length; i++) {
            var friend = response.friends.data[i];
            FB.api('/' + response.friends.data[i].id + '/picture', function(pictureResponse) {
                if (pictureResponse && !pictureResponse.error) {
                    friend.picture = pictureResponse.data.url;
                    friends.push(friend);
                    console.log(friend);
;                }
            }, {
                type: 'normal'
            });
        }
    });
}

function login() {
    FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
            return onConnected();
        } else if (response.status === 'not_authorized') {
            // connected to facebook but app is not authorized.
        } else {
            // disconnected from facebook
        }

        FB.login(function(response) {
            return onConnected();
        }, {
            scope: 'email,user_friends'
        }, function() {
//            ngToast.create({
//                className: 'warning',
//                content: 'Você deve permitir que o facebook se conecte ao aplicativo para prosseguir.'
//            });
        });
    });

    // 20%, shape w: 24px h: 28px, margin: 0, 10px
}
