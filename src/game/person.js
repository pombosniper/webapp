var Person  = (function(_x,_y,_vx) {
	var pub = {};

	var SPRITE_WIDTH = 50;
	var SPRITE_HEIGHT = 100;
	var WORLD_WIDTH = 1000;
	var shape = Shape.Rectangle(_x,_y,SPRITE_WIDTH, SPRITE_HEIGHT,_vx,0.0);

	pub.updateFrame = function(delta_t) {
		shape.x += shape.vx*delta_t;
		shape.y += shape.vy*delta_y;
	}

	var onCollisionPeople = function(col_shape)
	{
		var dir_collision = (col_shape.x - shape.x)/Math.abs(col_shape.x - shape.x);
		shape.vx = -dir_collision*Math.abs(shape.vx);
	}

	function getPeopleAnimations() {
	    // define two animations, run (loops, 1.5x speed) and jump (returns to run):
	    return {
	        "run": [0, 25, "run", 1.5],
	        "jump": [26, 63, "run"]
	    }
	}

	function handleJumpStart(event) {
	  person.gotoAndPlay("jump");
	}
	pub.onCollisionPoop = function(col_shape)
	{
		// TODO
	}

	pub.onCollisionWall = function()
	{
		shape.vx = -shape.vx;
	}

	pub.checkCollisionPeople = function(list_people) {
		for (person in list_people) {
			if(person.hasCollision(shape)) {
				pub.onCollisionPeople(person.shape);
				person.onCollisionPeople(shape);
			}
		}
	}

	pub.checkCollisionFloor = function() {
		if(shape.x <= 0 || shape.x + shape.w >= WORLD_WIDTH) {
			pub.onCollisionWall();
		}
	}

	pub.hasCollision = function(col_shape) {
		return Shape.hasCollision(shape,col_shape);
	}

}
