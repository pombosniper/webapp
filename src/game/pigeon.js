var keys = {};
var pigeonAY = 0;
var pigeonJUMP = -300;
var POO_CD_MS = 200;
var JUMP_CD_MS = 200;

var game_started = false;

function getPigeonAssets() {
  return [{src: "Pomba.png", id: "pomba"}];
}

function loadPigeon() {
  var spriteSheet = new createjs.SpriteSheet({
      framerate: 30,
      "images": [loader.getResult("pomba")],
      "frames": { "height": 57, "width": 45},
      "animations": getPigeonAnimations()
    });

  var pigeon = new createjs.Sprite(spriteSheet, "run");
  pigeon.x = 50;
  pigeon.y = 30;
  pigeon.vy = 0;
  pigeon.poo = [];
  pigeon.canpoop = true;
  pigeon.canjump = true;

  stage.addChild(pigeon);

  this.document.onkeydown = keyDown;
  this.document.onkeyup = keyUp;

  return pigeon;
}

function getPigeonAnimations() {
    // define two animations, run (loops, 1.5x speed) and jump (returns to run):
    return {
        "run": [1, 3, "run", 0.2],
        "run2": [1, 3, "run2", 0.4],
    }
}

function updatePigeon(event, pigeon) {

    updatePoos(event);

    var deltaS = event.delta / 1000;
    pigeon.vy += pigeonAY*deltaS;
    pigeon.y = Math.max(-20,pigeon.y + pigeon.vy*deltaS);//Math.max(-10, newPositionY);

    var newPositionY = pigeon.y + pigeon.vy*deltaS;
    if (!checkPigeonCollisions()) {
      return;
    }

    if (keys[87] && pigeon.canjump) {

      if(!game_started)
      {
        game_started = true;
        pigeonAY = 800;
      }
      var soundpru = createjs.Sound.play(1);
      soundpru.volume = 0.5;

      pigeon.canjump = false;
        setTimeout(function(){
          pigeon.canjump = true;
        }, JUMP_CD_MS);

      if(pigeon.canpoop)
      {
        pigeon.canpoop = false;


          setTimeout(function(){
            pigeon.canpoop = true;
          }, POO_CD_MS);

        poo = loadPoo(pigeon);
        pigeon.poo.push(poo);
      }

      pigeon.vy = pigeonJUMP;
      pigeon.gotoAndPlay("run2");
      setTimeout(function() {
        pigeon.gotoAndPlay("run");
      }, 1000);

     // stage.update();
    }

    if(keys[68])
      {
              if(!game_started)
      {
        game_started = true;
        pigeonAY = 800;
      }
        pigeon.x = Math.min(pigeon.x + 250 * deltaS, 900);
      }
    if(keys[65]){
      if(!game_started)
      {
        game_started = true;
        pigeonAY = 800;
      }
      pigeon.x = Math.max(pigeon.x - 250 * deltaS, 25);
    }
}

function keyDown(event){
    keys[event.keyCode] = true;
}

function keyUp(event){

  delete(keys[event.keyCode]);

}

function checkPigeonCollisions() {
  if (!checkPigeonCollidingWithGround()) {
    console.log('ground')
    return false;
  }

  if (!checkPigeonCollidingWithBuildings()) {
    console.log('building')
    return false;
  }

  if (!checkPigeonCollidingWithDrone()) {
    console.log('drone')
    return false;
  }

  return true;
}

function checkPigeonCollidingWithGround() {
  var groundImg = loader.getResult("ground");
  var aaa = stage.canvas.height - groundImg.height;

    if(pigeon.y > aaa) {
      onPigeonCollision();
      return false;
    }

  return true;
}

function checkPigeonCollidingWithDrone() {
  if (!drone) {
    return true;
  }

  if(collisionManager.hasCollision(
      drone.x + drone.getBounds().width/4.0,
      drone.y + drone.getBounds().height/4.0,
      drone.getBounds().height/2.0 * drone.scaleY,
      drone.getBounds().width/2.0 * drone.scaleX,
      pigeon.x,
      pigeon.y,
      pigeon.getBounds().height * pigeon.scaleY,
      pigeon.getBounds().width * pigeon.scaleX)
  ) {
    onDroneCollision();
    onPigeonCollision();
    return false;
  }

  return true;
}

function checkPigeonCollidingWithBuildings() {
  for (var i = 0; i < buildings.length; i++) {
    var target = buildings[i];
    if(collisionManager.hasCollision(
        target.x,
        target.y,
        target.getBounds().height * target.scaleY,
        target.getBounds().width * target.scaleX,
        pigeon.x + pigeon.getBounds().width/4.0,
        pigeon.y + pigeon.getBounds().height/4.0,
        pigeon.getBounds().height/2.0 * pigeon.scaleY,
        pigeon.getBounds().width/2.0 * pigeon.scaleX)
    ) {
      onBuildingCollision(target);
      onPigeonCollision();
      return false;
    }
  }
  return true;
}

function onPigeonCollision() {
  running = false;
  endGame();
}
