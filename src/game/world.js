var sky, ground, hill, hill2, spawnDelay=0, spawnRate=0.5, speed = 1.5, HILL_SPEED = 30, HILL2_SPEED = 45, score = 0;
var GROUND_SPEED = 110;

function getWorldAssets() {
  return [
    {src: "sky.png", id: "sky"},
    {src: "ground.png", id: "ground"},
    {src: "hill1.png", id: "hill"},
    {src: "hill2.png", id: "hill2"},
    {src: "h1.png", id: "h1"},
    {src: "h2.png", id: "h2"}
  ];
}

function loadWorld() {
  var w = stage.canvas.width;
  var h = stage.canvas.height;

  sky = new createjs.Shape();
  sky.graphics.beginBitmapFill(loader.getResult("sky")).drawRect(0, 0, w, h);

  var groundImg = loader.getResult("ground");
  ground = new createjs.Shape();
  ground.graphics.beginBitmapFill(groundImg).drawRect(0, 0, w + groundImg.width, groundImg.height);
  ground.tileW = groundImg.width;
  ground.y = h - groundImg.height;

  hill0 = new createjs.Bitmap(loader.getResult("h2"));
  hill0.setTransform(3.0 *Math.random() * w, h - hill0.image.height * 2 - groundImg.height, 2, 2);
  hill0.y+=1;

  hill4 = new createjs.Bitmap(loader.getResult("h1"));
  hill4.setTransform(3.0 *Math.random() * w, h - hill4.image.height * 2 - groundImg.height, 2, 2);
  hill4.y+=1;

  hill = new createjs.Bitmap(loader.getResult("hill"));
  hill.setTransform(3.0 * Math.random() * w, h - hill.image.height * 0.4 - groundImg.height, 0.4, 0.4);
  //hill.alpha = 0.5;
  hill.y+=1;

  hill2 = new createjs.Bitmap(loader.getResult("hill2"));
  hill2.setTransform(3.0 * Math.random() * w, h - hill2.image.height * 0.6 - groundImg.height, 0.6, 0.6);

  //hill3 = new createjs.Bitmap(loader.getResult("hill2"));
  //hill3.setTransform(Math.random() * w, h - hill3.image.height * 0.6 - groundImg.height, 0.6, 0.6);

  stage.addChild(sky, hill0, hill4, hill, hill2, ground);
}

function updateWorld(event) {
  var w = stage.canvas.width;
  var h = stage.canvas.height;

  var deltaS = event.delta / 1000;

  ground.x = (ground.x - deltaS * speed * GROUND_SPEED) % ground.tileW;
  
  hill0.x = (hill0.x - deltaS * speed * HILL_SPEED);

  if (hill0.x + hill0.image.width * hill0.scaleX <= 0) {
    hill0.x = w;
  }

  hill.x = (hill.x - deltaS * speed * 0.8*HILL2_SPEED);

  if (hill.x + hill.image.width * hill.scaleX <= 0) {
    hill.x = w;
  }

  hill4.x = (hill4.x - deltaS * speed * HILL_SPEED);
  if (hill4.x + hill4.image.width * hill4.scaleX <= 0) {
    hill4.x = w;
  }

  hill2.x = (hill2.x - deltaS * speed * HILL2_SPEED);
  if (hill2.x + hill2.image.width * hill2.scaleX <= 0) {
    hill2.x = w;
  }


  // hill3.x = (hill3.x - deltaS * speed * HILL2_SPEED);
  // if (hill3.x + hill3.image.width * hill3.scaleX <= 0) {
  //   hill3.x = w;
  // }

  var factor = Math.random();
  if(game_started){
  if (targets.length == 0 || (factor < spawnRate * deltaS * speed && spawnDelay > 1))
  {
      targets.push(loadTarget());
      spawnDelay = 0;
  }
  spawnDelay += deltaS;
  if (speed < 5)
  {
    speed += deltaS * 0.01;
  }
  }
}
