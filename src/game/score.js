var score, scoreText;

function loadScore() {
	score = 0;

	scoreText = new createjs.Text("Score: 0", "bold 16px Arial", "#ff7700");
	scoreText.x = 30;//stage.canvas.width - 150; 
	scoreText.y = 30;
 	scoreText.textBaseline = "alphabetic";

 	stage.addChild(scoreText);
}

function addScore() {
	score += 10;
}

function updateScore() {
	scoreText.text = "Score: " + parseInt(score);
}