var drone;

function getDroneAssets() {
  return [{src: "Drone.png", id:"drone"}];
  //return [{src: "cocodebug.jpg", id: "drone"}];
}

function loadDrone() {
  var spriteSheet = new createjs.SpriteSheet({
      framerate: 30,
      "images": [loader.getResult("drone")],
      "frames": { "height": 64, "width": 106},
      "animations": getDroneAnimations()
    });

  drone = new createjs.Sprite(spriteSheet, "fixed");
  drone.x = stage.canvas.width;
  drone.scaleX = 0.7;
  drone.scaleY = 0.7;
  drone.y = 70;
  drone.vy = 0;
  drone.vx = -150 + Math.random()*100.0;
  drone.basey = Math.random()*100.0;
  drone.period = Math.random()*0.8+0.4;

  drone.currentY = 0.0;

  stage.addChild(drone);
}

function getDroneAnimations() {
    // define two animations, run (loops, 1.5x speed) and jump (returns to run):
    return {
        "fixed": [0, 0, "fixed"]
    }
}

function updateDrone(event) {
  var deltaS = event.delta / 1000;

  drone.currentY += 5.0*deltaS;
  drone.y = drone.basey + Math.sin(drone.currentY*drone.period) * 20.0;

  drone.vx = drone.vx + deltaS * -50;
  drone.x = drone.x + deltaS * drone.vx;
  if (drone.x < 0) {
    stage.removeChild(drone);
    drone = undefined;
  }
}

function onDroneCollision() {
  //
}

function checkCreateDrone() {
  if (Math.random() > 0.01) {
    return;
  }

  loadDrone();
}
