function getTargetAssets() {
  return [
    {src: "Boy_Reaction.png", id: "grant"},
    {src: "facebook2.png", id: "fb_friend"}
  ];
}

function loadTarget() {

  var w = stage.canvas.width;
  var h = stage.canvas.height;
  var spriteSheet = new createjs.SpriteSheet({
      framerate: 30,
      "images": [loader.getResult("grant")],
      "frames": {height: 80, width: 18},
      "animations": getTargetAnimations()
    });

  var target = new createjs.Sprite(spriteSheet, "run");
  target.x = w;
  target.y = 270 + Math.random()*67;
  target.scaleX = 0.7;
  target.scaleY = 0.7;

  stage.addChild(target);

  if (Math.random() < 0.2) {
    var fbSheet = new createjs.SpriteSheet({
      framerate: 30,
      "images": [loader.getResult("fb_friend")],
      "frames": {height: 34, width: 26},
      "animations": getTargetAnimations()
    });

    target.fb = new createjs.Sprite(fbSheet, "run");
    target.fb.x = target.x + target.getBounds().width/2 - target.fb.getBounds().width/2; 
    target.fb.y = target.y - 20;
    
    //target.displayName = new createjs.Text(friends[0].name.split(" ")[0], "bold 20px Arial", "#000000");
    target.displayName = new createjs.Text("Felipe", "bold 20px Arial", "#000000");
    var nameBounds = target.displayName.getBounds();
    target.displayName.x = target.x + target.getBounds().width/2 - nameBounds.width/2; 
    target.displayName.y = target.y - 20;
    target.displayName.textBaseline = "alphabetic";

    stage.addChild(target.fb, target.displayName);
  }

  return target;
}

function onCollisionPeople(target) {
  target.gotoAndPlay("jump");
  createjs.Sound.play(3);

  for(var t = 0; t < targets.length; ++t)
  {
    if(targets[t] != target)
    {
      var tgt = targets[t];
      if(Math.abs(target.x - tgt.x) < 100)
      {
        (function(t)
        {

          setTimeout(function(){
            targets[t].gotoAndPlay("jump");
          }, Math.random() * 1000);
        })(t);
      }
    }
  }
  return;dwdw
}

function getTargetAnimations() {
    // define two animations, run (loops, 1.5x speed) and jump (returns to run):
    return {
        "run": [0, 0, "run", 0.2],
        "jump": [1, 3, "jump", 0.4]
    }
}

function updateTarget(event, target) {
  var w = stage.canvas.width;
  var h = stage.canvas.height;

  var deltaS = event.delta / 1000;
  var factor = Math.random();

  var step = speed * (150 * deltaS);

  dir = 1;
  target.scaleX = dir;
  target.x -= step;

  if (target.displayName) {
    target.fb.x -= step; 
    target.displayName.x -= step;
  }

  if(target.x < -target.width)
  {
    targets.splice(targets.indexOf(target), 1);
  }
}
