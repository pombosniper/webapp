var collisionManager = loadCollisionManager();
var soundsEnabled = false;

var pigeon;
var targets = [];
var buildings = [];

var WORLD_SPEED = 50;

var bgm;

function initGame() {
  game_started = false;

        pigeonAY = 0;
  getSounds();
  loader.addEventListener("complete", handleComplete);
  loader.loadManifest(getManifest(), true, "_assets/art/");

  if(soundsEnabled)
  {
    bgm.play(2);
  }
}

function getManifest() {
  manifest = [];
  manifest = manifest.concat(getWorldAssets());
  manifest = manifest.concat(getPigeonAssets());
  manifest = manifest.concat(getPooAssets());
  manifest = manifest.concat(getTargetAssets());
  manifest = manifest.concat(getBuildingAssets());
  manifest = manifest.concat(getDroneAssets());

  return manifest;
}

function getSounds() {
  if (!createjs.Sound.initializeDefaultPlugins()) {
    console.log('errpr')
		return;
	}

	var assetsPath = "_assets/audio/";
	var sounds = [
		{src: "SuperPru.wav", id: 1},
    {src: "bgm.wav", id: 2},
    {src: "scream.wav", id: 3}
	];

	//createjs.Sound.alternateExtensions = ["wav", "mp3"];	// add other extensions to try loading if the src file extension is not supported
	createjs.Sound.addEventListener("fileload", createjs.proxy(soundLoaded, this)); // add an event listener for when load is completed
	createjs.Sound.registerSounds(sounds, assetsPath);
}

var bgm;

function soundLoaded(event) {

  bgm =  createjs.Sound.play(2, {loop:-1});
  console.log('sound loaded', event)
  if (bgm == null || bgm.playState == createjs.Sound.PLAY_FAILED) {
    return;
  }

  console.log('playoing')
  bgm.addEventListener("complete", function (bgm) {
    console.log('completed')
  });

  console.log('sound loaded', event)
  var instance = createjs.Sound.play(1);
  if (instance == null || instance.playState == createjs.Sound.PLAY_FAILED) {
		return;
	}

  console.log('playoing')
	instance.addEventListener("complete", function (instance) {
    bgm.play();
		console.log('completed')
	});


  console.log('sound loaded', event)
  var instance2 = createjs.Sound.play(3);
  if (instance2 == null || instance2.playState == createjs.Sound.PLAY_FAILED) {
    return;
  }

  console.log('playoing')
  instance2.addEventListener("complete", function (instance) {
    bgm.play();
    console.log('completed')
  });


  bgm.stop();

  soundsEnabled = true;
}

function handleComplete() {
  loadWorld();

  for (var i = 0; i < 5; i++) {
    buildings.push(loadBuilding());
  }

  loadScore();
  pigeon = loadPigeon();

  createjs.Ticker.timingMode = createjs.Ticker.RAF;
  createjs.Ticker.addEventListener("tick", tickGame);
}

function tickGame(event) {
  if (!running) {
  		stage.update(event);
      	return;
  }

  updateWorld(event);
  updatePigeon(event, pigeon);

  for (var i = 0; i < targets.length; i++) {
      updateTarget(event, targets[i]);
  }

  for (var i = 0; i < buildings.length; i++) {
      updateBuilding(event, buildings[i]);
  }

  if (drone || !game_started) {
    updateDrone(event);
  } else {
    checkCreateDrone();
  }

  updateScore();
  stage.update(event);
}

function cleanArray(actual) {
  var newArray = new Array();
  for (var i = 0; i < actual.length; i++) {
    if (actual[i]) {
      newArray.push(actual[i]);
    }
  }
  return newArray;
}
