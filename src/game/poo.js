var WORLD_HEIGHT = 300;
var pooAY = -150;//-100.0;
var pooVINICIALy = 500;//-300.0;
var pooVINICIALx = 0;//500.0;
var pooAX = 0;//-200.0;

function getPooAssets() {
  return [{src: "Poop.png", id: "poo"}];
}

function loadPoo(pigeon) {
  var spriteSheet = new createjs.SpriteSheet({
      framerate: 30,
      "images": [loader.getResult("poo")],
      "frames": {"height": 23, "width": 7},
      "animations": getPooAnimations()
    });

  var poo = new createjs.Sprite(spriteSheet, "run");
  poo.x = pigeon.x;
  poo.y = pigeon.y + pigeon.getBounds().height * pigeon.scaleY * 0.5;
  poo.scaleX = 2.0;
  poo.scaleY = 2.0; 
  poo.vy = pooVINICIALy;//Math.max(0, pigeon.vy);
  poo.vx = pooVINICIALx;//Math.max(0, pigeon.vy);

  stage.addChild(poo);

  return poo;
}

function getPooAnimations() {
    // define two animations, run (loops, 1.5x speed) and jump (returns to run):
    return {
      "run": [0, 2, "run2", 0.9],
      "run2": [2, 2, "run2"]
    }
}

function checkCollisionFloor(poo) {
  if (poo.y >= 300) {
    onCollisionFloor(poo);
    return false;
  }

  return true;
}

function onCollisionFloor(poo) {
  stage.removeChild(poo);
  poo = undefined;
}

function onCollisionPoop(poo) {
  stage.removeChild(poo);
  poo = undefined;
}

function checkCollisionPeople(poo) {
  for (var i = 0; i < targets.length; i++) {
    var target = targets[i];

      if(collisionManager.hasCollision(
          target.x,
          target.y,
          target.getBounds().height * target.scaleY,
          target.getBounds().width * target.scaleX,
          poo.x,
          poo.y,
          poo.getBounds().height * poo.scaleY,
          poo.getBounds().width * poo.scaleX)
      ) {
        addScore();
        onCollisionPeople(target);
        onCollisionPoop(poo);
        return false;
      }

  }
  return true;
}

function updatePoos(event) {
  var deltaS = event.delta / 1000;

  for (var i in pigeon.poo) {
    poo = pigeon.poo[i];
    poo.vy += pooAY*deltaS;
    poo.vx += pooAX*deltaS;
    var newPositionY = poo.y + poo.vy*deltaS;

    poo.x += poo.vx*deltaS;
    poo.y = newPositionY;

    if (!checkCollisionFloor(poo, newPositionY)) {
      continue;
    }

    if (!checkCollisionPeople(poo, newPositionY)) {
      continue;
    }


  }
  pigeon.poo = cleanArray(pigeon.poo);
}
