function getBuildingAssets() {
  return [
    {src: "Build1.png", id: "building1"},
    {src: "Build2.png", id: "building2"},
    {src: "Build3.png", id: "building3"}
  ];
}

function loadBuilding() {
  var w = stage.canvas.width;
  var h = stage.canvas.height;

  var spriteSheet = new createjs.SpriteSheet({
      framerate: 30,
      "images": [loader.getResult("building1")],
      "frames": { "height": 500, "width": 280},
      "animations": getBuildingAnimations()
    });

  var groundImg = loader.getResult("ground");

  var factor = Math.round(Math.random() * 1.5) + 2;

  var building = new createjs.Sprite(spriteSheet, "fixed");
  building.x = Math.random() * w;
  building.y = h - 0.3 * building.getBounds().height - groundImg.height;
  building.scaleX = 0.3;
  building.scaleY = 0.3;
  building.vy = 0;

  stage.addChild(building);

  return building;
}

function loadBuilding2() {
  var w = stage.canvas.width;
  var h = stage.canvas.height;

  var spriteSheet = new createjs.SpriteSheet({
      framerate: 30,
      "images": [loader.getResult("building2")],
      "frames": { "height": 780, "width": 550},
      "animations": getBuildingAnimations()
    });

  var groundImg = loader.getResult("ground");

  var factor = Math.round(Math.random() * 1.5) + 2;

  var building = new createjs.Sprite(spriteSheet, "fixed");
  building.x = Math.random() * w;
  building.y = h - 0.25 * building.getBounds().height - groundImg.height;
  building.scaleX = 0.25;
  building.scaleY = 0.25;
  building.vy = 0;

  stage.addChild(building);

  return building;
}

function loadBuilding3() {
  var w = stage.canvas.width;
  var h = stage.canvas.height;

  var spriteSheet = new createjs.SpriteSheet({
      framerate: 30,
      "images": [loader.getResult("building3")],
      "frames": { "height": 400, "width": 520},
      "animations": getBuildingAnimations()
    });

  var groundImg = loader.getResult("ground");

  var factor = Math.round(Math.random() * 1.5) + 2;

  var building = new createjs.Sprite(spriteSheet, "fixed");
  building.x = Math.random() * w;
  building.y = h - 0.3 * building.getBounds().height - groundImg.height;
  building.scaleX = 0.3;
  building.scaleY = 0.3;
  building.vy = 0;

  stage.addChild(building);

  return building;
}

function getBuildingAnimations() {
    // define two animations, run (loops, 1.5x speed) and jump (returns to run):
    return {
        "fixed": [0, 0, "fixed"]
    }
}

function updateBuilding(event, building) {
  var w = stage.canvas.width;
  var h = stage.canvas.height;

  var deltaS = event.delta / 1000;

  building.x = building.x - deltaS * speed* GROUND_SPEED;

  if (building.x + building.getBounds().width * building.scaleX <= 0) {
    building.x = w;
  }
}

function onBuildingCollision(building) {
  //
}
