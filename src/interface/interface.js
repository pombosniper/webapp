function initInterface() {
	manifest = [];
	manifest = manifest.concat(getWorldAssets());

	loader.addEventListener("complete", onInterfaceLoaded);
	loader.loadManifest(manifest, true, "_assets/art/");
}

function onInterfaceLoaded() {
	started = false;
	game_started = false;

        pigeonAY = 0;

	stage.removeAllChildren();
	stage.removeAllEventListeners();
	createjs.Ticker.timingMode = createjs.Ticker.RAF;
	createjs.Ticker.addEventListener("tick", tickInterface);

  var w = stage.canvas.width;
  var h = stage.canvas.height;

  sky = new createjs.Shape();
  sky.graphics.beginBitmapFill(loader.getResult("sky")).drawRect(0, 0, w, h);

  var groundImg = loader.getResult("ground");
  ground = new createjs.Shape();
  ground.graphics.beginBitmapFill(groundImg).drawRect(0, 0, w + groundImg.width, groundImg.height);
  ground.tileW = groundImg.width;
  ground.y = h - groundImg.height;

  hill0 = new createjs.Bitmap(loader.getResult("h2"));
  hill0.setTransform(0.1 * w, h - hill0.image.height * 2 - groundImg.height, 2, 2);
  hill0.y+=1;

  hill4 = new createjs.Bitmap(loader.getResult("h1"));
  hill4.setTransform(0.7 * w, h - hill4.image.height * 2 - groundImg.height, 2, 2);
  hill4.y+=1;

  hill = new createjs.Bitmap(loader.getResult("hill"));
  hill.setTransform(0.8 * w, h - hill.image.height * 0.4 - groundImg.height, 0.4, 0.4);
  //hill.alpha = 0.5;
  hill.y+=1;

  hill2 = new createjs.Bitmap(loader.getResult("hill2"));
  hill2.setTransform(0.1 * w, h - hill2.image.height * 0.6 - groundImg.height, 0.6, 0.6);

  //hill3 = new createjs.Bitmap(loader.getResult("hill2"));
  //hill3.setTransform(Math.random() * w, h - hill3.image.height * 0.6 - groundImg.height, 0.6, 0.6);

  stage.addChild(sky, hill0, hill4, hill, hill2, ground);


	var width = stage.canvas.width / 2, height = stage.canvas.height / 2;

	var text = new createjs.Text(" Estou cagando pra você, clica aí vai!\n                Pruuuu... Pruuu...\n\n(Tap [W] para voar e [A][D] para mover.)", "bold 32px Arial", "#ff7700");
 	var b = text.getBounds();
	text.x = width - b.width/2; 
	text.y = height - b.height/2 - 100;
 	text.textBaseline = "alphabetic";

 	stage.addChild(text);

 	stage.addEventListener("stagemousedown", manageClick);
}

function manageClick() {
	if (!running) {
		if (started) {
			return onInterfaceLoaded();
		}

		return startGame();
	}
}

function startGame() {
	cleanArray(targets);
	cleanArray(buildings);
	cleanArray(buildings);
	targets = [];
	buildings = [];
	drone = undefined;
	stage.removeAllChildren();
	stage.removeAllEventListeners();

	initGame();

	started = true;
	running = true;
}

function endGame() {
	var width = stage.canvas.width / 2, height = stage.canvas.height / 2;
stage.removeAllChildren();
	stage.removeAllEventListeners();

	var text = new createjs.Text("\n\n            GAME OVER!\n\n\n(clique para reiniciar o jogo)", "bold 32px Arial", "#ff7700");
 	var b = text.getBounds();
	text.x = width - b.width/2; 
	text.y = 120;
 	text.textBaseline = "alphabetic";

 	running = false;
 	stage.addChild(text);
 	stage.addEventListener("stagemousedown", onInterfaceLoaded);
 	bgm.stop();
}

function tickInterface(event) {
    stage.update(event);
}
