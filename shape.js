var Shape = (function() {
	var pub = {};

	pub.Rectangle = function (_x, _y, _w, _h, _vx, _vy) {
		var rectangle = {}
		rectangle.type = "rec";
		rectangle.x = _x;
		rectangle.y = _y;
		rectangle.w = _w;
		rectangle.h = _h;
		rectangle.vx = _vx;
		rectangle.vy = _vy;
		return rectangle;
	}

	pub.Circle = function (_x, _y, _r, _vx, _vy) {
		var circle = {}
		circle.type = "cir";
		cirlce.x = _x;
		cirlce.y = _y;
		cirlce.r = _r;
		circle.vx = _vx;
		circle.vy = _vy;
		return circle;
	}

	var hasCollisionCC = function (cir_a, cir_b) {
		var delta_x = cir_b.x - cir_a.x;
		var delta_y = cir_b.y - cir_a.y;
		var sum_radius = cir_a.r + cir_b.r;

		var dist_squared = delta_x*delta_x + delta_y*delta_y;
		var radius_squared = sum_radius*sum_radius;

		if(dist_squared <= radius_squared) {
			return true;
		}
		return false;
	}

	var hasCollisionRR = function (rec_a, rec_b) {
		var a_left = rec_a.x;
		var a_right = rec_a.x+rec_a.w;
		var a_top = rec_a.y;
		var a_bot = rec_a.y+rec_a.h;

		var b_left = rec_b.x;
		var b_right = rec_b.x+rec_b.w;
		var b_top = rec_b.y;
		var b_bot = rec_b.y+rec_b.h;

		var cond_1 = a_left < b_right;
		var cond_2 = a_right > b_left;
		var cond_3 = a_top < b_bot;
		var cond_4 = a_bot > b_top;

		if(cond_1 && cond_2 && cond_3 && cond_4) {
			return true;
		}
		return false;
	}

	var hasCollisionCR = function(cir, rec) {
		// TODO
		var rec_approx_for_cir = pub.Rectangle(cir.x-cir.r/2.0,cir.y-cir.r/2.0,2.0*r,2.0*r);
		return pub.hasCollisionRR(rec,rec_approx_for_cir);
	}

	pub.hasCollision = function(shape_a,shape_b) {
		if(shape_a.type==="cir" && shape_b.type==="cir") {
			return hasCollisionCC(shape_a,shape_b);
		}
		if(shape_a.type==="rec" && shape_b.type==="rec") {
			return hasCollisionCC(shape_a,shape_b);
		}
		if(shape_a.type==="cir" && shape_b.type==="rec") {
			return hasCollisionCR(shape_a,shape_b);
		}
		if(shape_a.type==="rec" && shape_b.type==="cir") {
			return hasCollisionCR(shape_b,shape_a);
		}
	}
})
